import pandas as pd


def GetColumnKeys(Truth, Reference):
	keyOutput = []
	TruthColumnList = Truth.columns
	ReferenceColumnList = Reference.columns
	for truthColumn in TruthColumnList:
		found = False
		for referenceIndex, referenceColumn in enumerate(ReferenceColumnList):
			if(truthColumn == referenceColumn):
				keyOutput.append(referenceIndex)
				found = True
				break
		if(not found):
			keyOutput.append(-1)
	return keyOutput

def GetRowKeys(Truth, Reference, idRow):
	TruthIDRowList = Truth.loc[:,idRow]
	ReferenceIDRowList = Reference.loc[:,idRow]
	keyOutput = []
	for truthColumn in TruthIDRowList:
		found = False
		for referenceIndex, referenceColumn in enumerate(ReferenceIDRowList):
			if(truthColumn == referenceColumn):
				keyOutput.append(referenceIndex)
				found = True
				break
		if(not found):
			keyOutput.append(-1)
	return keyOutput
	

def getOutputColumns(truthFileDF, ColumnKeys):
	outCol = []
	for truthColumnIndex, referenceColumnIndex in enumerate(ColumnKeys):
		if(referenceColumnIndex == -1):
			continue
		outCol.append(truthFileDF.columns[truthColumnIndex])
	for truthColumnIndex, referenceColumnIndex in enumerate(ColumnKeys):
		if(referenceColumnIndex != -1):
			continue
		outCol.append(truthFileDF.columns[truthColumnIndex])
	return outCol

def EqualizeRows(outputRows, comparedCount):
	tempRows = [0] * comparedCount
	for truthColumnIndex, referenceColumnIndex in enumerate(ColumnKeys):
		if(referenceColumnIndex != -1):
			continue
		outputRows.append(tempRows)

	return outputRows

def getOutputRows(truthFileDF, referenceFileDF, ColumnKeys, RowKeys):
	outputRows = []
	
	for columnKeyIndex, columnKey in enumerate(ColumnKeys):
		comparedCount = 0
		outRow = []
		if(columnKey == -1):
			continue
		for rowKeyIndex, rowKey in enumerate(RowKeys):
			if(rowKey == -1):
				continue
			if(truthFileDF[truthFileDF.columns[columnKeyIndex]][rowKeyIndex] == referenceFileDF[referenceFileDF.columns[columnKey]][rowKey]):
				outRow.append(True)
			else:
				outRow.append(False)
			comparedCount = comparedCount + 1
		outputRows.append(outRow)
	outputRows = EqualizeRows(outputRows, comparedCount)
	return outputRows
		


truthFileName = "Truth.csv" 
referenceFileName = "Reference.csv" 
outputFileName = "Output.csv"
idRow = "ID"

referenceFileDF = pd.read_csv(referenceFileName,  encoding="utf-8")
truthFileDF = pd.read_csv(truthFileName,  encoding="utf-8")

input("Press a button to continue...")


ColumnKeys = GetColumnKeys(truthFileDF, referenceFileDF)
RowKeys = GetRowKeys(truthFileDF, referenceFileDF, idRow)

outputColumns = getOutputColumns(truthFileDF, ColumnKeys)
outputRows = getOutputRows(truthFileDF, referenceFileDF, ColumnKeys, RowKeys)

input("Press a button to continue...")

outputDataDic = {}
for colIndex, column in enumerate(outputColumns):
	outputDataDic[column] = outputRows[colIndex]
#outputFile = pd.DataFrame(outputRows, columns = outputColumns)
outputFile = pd.DataFrame(outputDataDic)
outputFile.to_csv(outputFileName, index=False)







'''
ColumnKeys = [1,-1,4,5]
T = [A,B,C,D]
R = [E,A,F,H,C,D]
'''

	    
